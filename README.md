# general-derivative

Tool to compute and plot derivatives of complex order except for negative integer orders, based on a generalization of
[Cauchy's differentiation formula](https://en.wikipedia.org/wiki/Cauchy%27s_integral_formula) that uses the gamma
function to generalize the factorial.

## Graphing
In `src/bin/derivative.rs`:
 - Set `MIN` and `MAX` to your desired minimum and maximum real input values to graph the derivative between
 - Set `STEPS` to the number of points between `MIN` and `MAX` to evaluate the derivative at
 - Set `ORDER` to the desired order of the derivative
 - Modify `to_derive` to be the function you wish to take the derivative of
 - Build and run the binary with `cargo run --bin derivative --release`

## Animation showing change in order of derivative
In `src/bin/derivatie_animate.rs`:
 - Set `X_MIN` and `X_MAX` to your desired minimum and maximum real input values to graph the derivative between
 - Set `X_STEPS` to the number of points between `X_MIN` and `X_MAX` to evaluate the derivative at
 - Set `ORDER_MIN` and `ORDER_MAX` to your desired minimum and maximum real input values to the order function
 - Set `ORDER_STEPS` to the number of points between `ORDER_MIN` and `ORDER_MAX` to animate; this will be the number of
frames in the animation
 - Modify `order` to be a function that takes a real number between `X_MIN` and `X_MAX` and returns the complex order of
a derivative. Negative integers cannot be used as complex orders of a derivative.
 - Set `FRAME_DELAY_MS` to the number of milliseconds each frame should display for.
 - Modify `to_derive` to be the function you wish to animate the changing derivative of

## Gamma implementation
The gamma function is calculated with an
[infinite product](https://en.wikipedia.org/wiki/Gamma_function#Euler's_definition_as_an_infinite_product) discovered by
Euler. I found 60,000 terms to be more than accurate enough for my testing. There almost certainly exist better
approximations for the Gamma function, so I don't recommend using this implementation.

## Integral implementation
Definite integrals are evaluated with a midpoint Riemann sum. I found 14,000 partitions to be more than accurate enough
for my testing, but this would certainly vary based on bounds of integration and the integrand. There are better
approximations for definite integrals, so I don't recommend using this implementation.
