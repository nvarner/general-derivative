use general_derivative::derivative;
use num_complex::Complex64;
use plotters::prelude::*;
use std::cmp::Ordering;
use std::error::Error;
// use std::f64::consts::TAU;

const GAMMA_ITERATIONS: usize = 60000;
const INTEGRAL_NUM_PARTS: usize = 14000;

// const MIN: f64 = 0.;
// const MAX: f64 = TAU;
const MIN: f64 = -10.;
const MAX: f64 = 10.;
const STEPS: usize = 100;

const ORDER: Complex64 = Complex64::new(0.75, 0.);

fn to_derive(z: Complex64) -> Complex64 {
    z
}

fn point_for_step(step: usize) -> f64 {
    MIN + (step as f64) * ((MAX - MIN) / (STEPS as f64))
}

fn compare_float(a: &f64, b: &f64) -> Ordering {
    if a == b {
        Ordering::Equal
    } else if a < b {
        Ordering::Less
    } else {
        Ordering::Greater
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let mut points = Vec::new();
    for step in 0..=STEPS {
        let point = point_for_step(step);
        let derivative_estimate = derivative(
            to_derive,
            Complex64::new(point, 0.0),
            ORDER,
            GAMMA_ITERATIONS,
            INTEGRAL_NUM_PARTS,
        );
        points.push((point, derivative_estimate));
        println!(
            "Derivative at {:.3}: {:.5}{:+.5}i",
            point, derivative_estimate.re, derivative_estimate.im
        );
    }

    let ys = points.iter().map(|(_x, y)| *y);
    let min_y_re = ys.clone().map(|y| y.re).min_by(compare_float).unwrap_or(0.);
    let min_y_im = ys.clone().map(|y| y.im).min_by(compare_float).unwrap_or(0.);
    let min_y = min_y_re.min(min_y_im);
    let max_y_re = ys.clone().map(|y| y.re).max_by(compare_float).unwrap_or(0.);
    let max_y_im = ys.map(|y| y.im).max_by(compare_float).unwrap_or(0.);
    let max_y = max_y_re.max(max_y_im);

    let root = BitMapBackend::new("plots/derivative.png", (800, 600)).into_drawing_area();
    root.fill(&WHITE)?;
    let mut chart = ChartBuilder::on(&root)
        .caption("Derivative", ("sans-serif", 50).into_font())
        .margin(5)
        .x_label_area_size(30)
        .y_label_area_size(50)
        .right_y_label_area_size(50)
        .build_cartesian_2d(MIN..MAX, (min_y - 0.5)..(max_y + 0.5))?
        .set_secondary_coord(0.0..1.0, -0.65..-0.6);

    chart
        .configure_mesh()
        // .disable_x_mesh()
        // .disable_y_mesh()
        // .y_desc("Real")
        .draw()?;

    // chart
    //     .configure_secondary_axes()
    //     .y_desc("Imaginary")
    //     .draw()?;

    chart
        .draw_series(LineSeries::new(
            points.iter().map(|(x, y)| (*x, y.re)),
            &RED,
        ))?
        .label("real part")
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &RED));

    chart
        // .draw_secondary_series(LineSeries::new(
        .draw_series(LineSeries::new(
            points.iter().map(|(x, y)| (*x, y.im)),
            &BLUE,
        ))?
        .label("imaginary part")
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &BLUE));

    chart
        .configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .draw()?;

    root.present()?;

    Ok(())
}
