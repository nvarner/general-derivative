use general_derivative::integral;
use num_complex::Complex64;
use plotters::prelude::*;
use std::error::Error;
use std::f64::consts::TAU;

const MAX_INTEGRAL_NUM_PARTS: usize = 200000;

fn test_integral(
    integral_num_parts: usize,
    input_expected: &Vec<(&dyn Fn(f64) -> Complex64, (f64, f64), Complex64)>,
) -> f64 {
    let num_inputs = input_expected.len();

    let expected = input_expected
        .iter()
        .map(|(_integrand, _bounds, expected)| expected);
    let inputs = input_expected
        .iter()
        .map(|(integrand, bounds, _expected)| (integrand.clone(), bounds));

    let actual = inputs.map(|(integrand, bounds)| integral(integrand, &bounds, integral_num_parts));

    let errors = actual
        .zip(expected)
        .map(|(actual, expected)| (actual - expected).norm());

    errors.sum::<f64>() / (num_inputs as f64)
}

fn main() -> Result<(), Box<dyn Error>> {
    // From https://tutorial.math.lamar.edu/classes/calcI/computingdefiniteintegrals.aspx
    let f1 = |x: f64| Complex64::new(x * x, 0.);
    let f2 = |_x: f64| Complex64::new(0., 0.);
    let f3 = |x: f64| Complex64::new(6. * x * x - 5. * x + 2., 0.);
    let f4 = |t: f64| Complex64::new(t.sqrt() * (t - 2.), 0.);
    let f5 = |w: f64| Complex64::new((2. * w * w * w * w * w - w + 3.) / (w * w), 0.);
    let f6 = |x: f64| Complex64::new((4. * x) - (6. * x.powf(2. / 3.)), 0.);
    let f7 = |t: f64| Complex64::new((2. * t.sin()) - (5. * t.cos()), 0.);
    let f8 = |z: f64| Complex64::new(5. - 2. * (1. / z.cos()) * z.tan(), 0.);
    let f9 = |z: f64| Complex64::new((3. / (-z).exp()) - (1. / (3. * z)), 0.);

    let input_expected: Vec<(&dyn Fn(f64) -> Complex64, _, _)> = vec![
        (&f1, (1., 2.), Complex64::new(7. / 3., 0.)),
        (&f2, (-5., 2.5), Complex64::new(0., 0.)),
        (&f3, (-3., 1.), Complex64::new(84., 0.)),
        (&f4, (4., 0.), Complex64::new(-32. / 15., 0.)),
        (&f5, (1., 2.), Complex64::new(9. - (2_f64).ln(), 0.)),
        (&f6, (0., 1.), Complex64::new(-8. / 5., 0.)),
        (
            &f7,
            (0., TAU / 6.),
            Complex64::new(1. - (75_f64.sqrt() / 2.), 0.),
        ),
        (
            &f8,
            (TAU / 12., TAU / 8.),
            Complex64::new((5. / 24.) * TAU - 8_f64.sqrt() + (4. / 3_f64.sqrt()), 0.),
        ),
        (
            &f9,
            (-20., -1.),
            Complex64::new(
                3. * (-1_f64).exp() - 3. * (-20_f64).exp() + (1. / 3.) * 20_f64.ln(),
                0.,
            ),
        ),
    ];

    let steps = 200;

    let mut estimates = Vec::new();
    for accuracy in 1..=steps {
        let accuracy_prop = (accuracy as f64) / (steps as f64);
        let integral_num_parts = (accuracy_prop * (MAX_INTEGRAL_NUM_PARTS as f64)) as usize;

        let error = test_integral(integral_num_parts, &input_expected);
        estimates.push((integral_num_parts, error));
        println!("Accuracy: {:3}, error: {:.5}", integral_num_parts, error);
    }

    let root = BitMapBackend::new("plots/integral_accuracy.png", (800, 600)).into_drawing_area();
    root.fill(&WHITE)?;
    let mut chart = ChartBuilder::on(&root)
        .caption("Integral Error", ("sans-serif", 50).into_font())
        .margin(5)
        .x_label_area_size(30)
        .y_label_area_size(50)
        .build_cartesian_2d(0.0..(MAX_INTEGRAL_NUM_PARTS as f64), 0.0..0.00001)?;

    chart.configure_mesh().draw()?;

    chart
        .draw_series(LineSeries::new(
            estimates
                .iter()
                .map(|(accuracy, error)| (*accuracy as f64, *error)),
            &RED,
        ))?
        .label("Integral error")
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &RED));

    chart
        .configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .draw()?;

    root.present()?;

    Ok(())
}
