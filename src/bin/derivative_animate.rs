use general_derivative::derivative;
use num_complex::Complex64;
use plotters::prelude::*;
use std::cmp::Ordering;
use std::error::Error;

const GAMMA_ITERATIONS: usize = 60000;
const INTEGRAL_NUM_PARTS: usize = 14000;

fn from_step(step: usize, steps: usize, min: f64, max: f64) -> f64 {
    min + (step as f64) * ((max - min) / (steps as f64))
}

const X_MIN: f64 = -3.;
const X_MAX: f64 = 3.;
const X_STEPS: usize = 100;
fn point_for_step(step: usize) -> f64 {
    from_step(step, X_STEPS, X_MIN, X_MAX)
}

const ORDER_MIN: f64 = 0.;
const ORDER_MAX: f64 = 2.;
const ORDER_STEPS: usize = 20;
fn order(t: f64) -> Complex64 {
    Complex64::new(0., t)
}
fn order_t_for_step(step: usize) -> f64 {
    from_step(step, ORDER_STEPS, ORDER_MIN, ORDER_MAX)
}

const FRAME_DELAY_MS: u32 = 100;

fn to_derive(z: Complex64) -> Complex64 {
    z.exp()
}

fn compare_float(a: &f64, b: &f64) -> Ordering {
    if a == b {
        Ordering::Equal
    } else if a < b {
        Ordering::Less
    } else {
        Ordering::Greater
    }
}

fn points_for_order(order: Complex64) -> impl Iterator<Item = (f64, Complex64)> {
    let points = (0..=X_STEPS).map(point_for_step);

    let estimates = points.clone().map(move |point| {
        derivative(
            to_derive,
            Complex64::new(point, 0.0),
            order,
            GAMMA_ITERATIONS,
            INTEGRAL_NUM_PARTS,
        )
    });

    points.zip(estimates)
}

fn main() -> Result<(), Box<dyn Error>> {
    let orders = (0..=ORDER_STEPS).map(order_t_for_step).map(order);
    let frames = orders.clone().map(points_for_order);

    let ys = frames
        .clone()
        .flatten()
        .map(|(_x, y)| y)
        .collect::<Vec<_>>();
    let min_y_re = ys.iter().map(|y| y.re).min_by(compare_float).unwrap_or(0.);
    let min_y_im = ys.iter().map(|y| y.im).min_by(compare_float).unwrap_or(0.);
    let min_y = min_y_re.min(min_y_im);
    let max_y_re = ys.iter().map(|y| y.re).max_by(compare_float).unwrap_or(0.);
    let max_y_im = ys.iter().map(|y| y.im).max_by(compare_float).unwrap_or(0.);
    let max_y = max_y_re.max(max_y_im);

    let root =
        BitMapBackend::gif("plots/derivative.gif", (800, 600), FRAME_DELAY_MS)?.into_drawing_area();

    for (frame, order) in frames.zip(orders) {
        println!("Starting frame of order {:.5}{:+.5}i", order.re, order.im);
        let frame = frame.collect::<Vec<_>>();
        root.fill(&WHITE)?;

        let mut chart = ChartBuilder::on(&root)
            .caption(
                format!("Derivative of order {:.5}{:+.5}i", order.re, order.im),
                ("sans-serif", 50).into_font(),
            )
            .margin(5)
            .x_label_area_size(30)
            .y_label_area_size(50)
            .right_y_label_area_size(50)
            .build_cartesian_2d(X_MIN..X_MAX, (min_y - 0.5)..(max_y + 0.5))?
            .set_secondary_coord(0.0..1.0, -0.65..-0.6);

        chart
            .configure_mesh()
            // .disable_x_mesh()
            // .disable_y_mesh()
            // .y_desc("Real")
            .draw()?;

        // chart
        //     .configure_secondary_axes()
        //     .y_desc("Imaginary")
        //     .draw()?;

        chart
            .draw_series(LineSeries::new(frame.iter().map(|(x, y)| (*x, y.re)), &RED))?
            .label("real part")
            .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &RED));

        chart
            // .draw_secondary_series(LineSeries::new(
            .draw_series(LineSeries::new(
                frame.iter().map(|(x, y)| (*x, y.im)),
                &BLUE,
            ))?
            .label("imaginary part")
            .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &BLUE));

        chart
            .configure_series_labels()
            .background_style(&WHITE.mix(0.8))
            .border_style(&BLACK)
            .draw()?;

        root.present()?;
    }

    root.present()?;

    Ok(())
}
