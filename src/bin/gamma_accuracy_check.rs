use general_derivative::gamma;
use num_complex::Complex64;
use plotters::prelude::*;
use std::error::Error;
use std::f64::consts::PI;

const MAX_GAMMA_ITERATIONS: usize = 200000;

fn test_gamma(iterations: usize, input_expected: &[(Complex64, Complex64)]) -> f64 {
    let inputs = input_expected.iter().map(|(input, _expected)| input);
    let expected = input_expected.iter().map(|(_input, expected)| expected);

    let actual = inputs.map(|input| gamma(*input, iterations));

    let errors = actual
        .zip(expected)
        .map(|(actual, expected)| (actual - expected).norm());

    errors.sum::<f64>() / (input_expected.len() as f64)
}

fn main() -> Result<(), Box<dyn Error>> {
    // From https://en.wikipedia.org/wiki/Particular_values_of_the_gamma_function
    let input_expected = vec![
        (Complex64::new(1., 0.), Complex64::new(1., 0.)),
        (Complex64::new(4., 0.), Complex64::new(6., 0.)),
        (Complex64::new(1.0 / 2.0, 0.), Complex64::new(PI.sqrt(), 0.)),
        (
            Complex64::new(7. / 2., 0.),
            Complex64::new((15. / 8.) * PI.sqrt(), 0.),
        ),
        (
            Complex64::new(-1. / 2., 0.),
            Complex64::new(-2. * PI.sqrt(), 0.),
        ),
        (
            Complex64::new(-3. / 2., 0.),
            Complex64::new((4. / 3.) * PI.sqrt(), 0.),
        ),
        (
            Complex64::new(1.0 / 8.0, 0.),
            Complex64::new(7.5339415987976119047, 0.),
        ),
        (Complex64::new(0., 1.), Complex64::new(-0.1549, -0.498)),
        (Complex64::new(1., -1.), Complex64::new(0.498, 0.1549)),
        (
            Complex64::new(1. / 2., 1. / 2.),
            Complex64::new(0.8181639995, -0.7633138287),
        ),
        (
            Complex64::new(5., -3.),
            Complex64::new(0.0160418827, 9.4332932898),
        ),
    ];

    let steps = 200;

    let mut estimates = Vec::new();
    for accuracy in 0..=steps {
        let accuracy_prop = (accuracy as f64) / (steps as f64);
        let gamma_iterations = (accuracy_prop * (MAX_GAMMA_ITERATIONS as f64)) as usize;

        let gamma_error = test_gamma(gamma_iterations, &input_expected);
        estimates.push((gamma_iterations, gamma_error));
        println!(
            "Iterations: {:3}, Error: {:.5}",
            gamma_iterations, gamma_error
        );
    }

    let root = BitMapBackend::new("plots/gamma_accuracy.png", (800, 600)).into_drawing_area();
    root.fill(&WHITE)?;
    let mut chart = ChartBuilder::on(&root)
        .caption("Gamma Error", ("sans-serif", 50).into_font())
        .margin(5)
        .x_label_area_size(30)
        .y_label_area_size(50)
        .build_cartesian_2d(0.0..(MAX_GAMMA_ITERATIONS as f64), 0.0..0.02)?;

    chart.configure_mesh().draw()?;

    chart
        .draw_series(LineSeries::new(
            estimates
                .iter()
                .map(|(accuracy, error)| (*accuracy as f64, *error)),
            &RED,
        ))?
        .label("Gamma error")
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &RED));

    chart
        .configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .draw()?;

    root.present()?;

    Ok(())
}
