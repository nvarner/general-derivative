use std::f64::consts::TAU;

use num_complex::Complex64;

struct PairWithNext<T, U: Iterator<Item = T>> {
    iter: U,
    last: Option<T>,
}

impl<T, U: Iterator<Item = T>> PairWithNext<T, U> {
    pub fn new(mut iter: U) -> Self {
        let last = iter.next();
        Self { iter, last }
    }
}

impl<T: Copy, U: Iterator<Item = T>> Iterator for PairWithNext<T, U> {
    type Item = (T, T);

    fn next(&mut self) -> Option<Self::Item> {
        let next = self.iter.next();
        let pair = self.last.and_then(|last| next.map(|next| (last, next)));
        self.last = next;
        pair
    }
}

/// Calculates a term of Euler's
/// [infinite product](https://en.wikipedia.org/wiki/Gamma_function#Euler's_definition_as_an_infinite_product)
/// definition of the Gamma function. Found on Wikipedia.
fn gamma_term(z: Complex64, n: f64) -> Complex64 {
    let numerator = z.expf(1. + (1. / n));
    let denominator = 1. + (z / n);
    return numerator / denominator;
}

/// Estimates the Gamma function. Based on Euler's
/// [infinite product](https://en.wikipedia.org/wiki/Gamma_function#Euler's_definition_as_an_infinite_product).
/// Found on Wikipedia.
pub fn gamma(z: Complex64, iterations: usize) -> Complex64 {
    let product: Complex64 = (1..=iterations).map(|n| gamma_term(z, n as f64)).product();
    product / z
}

fn dx_equal_partitions((a, b): &(f64, f64), partition_order: usize) -> f64 {
    (b - a) / ((partition_order - 1) as f64)
}

fn equal_partitions(a: f64, dx: f64, partition_order: usize) -> impl Iterator<Item = f64> {
    (0..partition_order).map(move |n| a + ((n as f64) * dx))
}

fn midpoints(partitions: impl Iterator<Item = f64>) -> impl Iterator<Item = f64> {
    PairWithNext::new(partitions).map(|(a, b)| (a + b) / 2.)
}

pub fn integral<T: Fn(f64) -> Complex64>(
    integrand: T,
    bounds: &(f64, f64),
    num_parts: usize,
) -> Complex64 {
    let dx = dx_equal_partitions(bounds, num_parts);
    let partition = equal_partitions(bounds.0, dx, num_parts);
    midpoints(partition).map(|m| integrand(m) * dx).sum()
}

struct DifferentiableContour<T: Fn(f64) -> Complex64, U: Fn(f64) -> Complex64> {
    pub parameterization: T,
    pub parameterization_prime: U,
    pub bounds: (f64, f64),
}

fn contour_integral<
    T: Fn(Complex64) -> Complex64,
    U: Fn(f64) -> Complex64,
    V: Fn(f64) -> Complex64,
>(
    integrand: T,
    contour: DifferentiableContour<U, V>,
    num_parts: usize,
) -> Complex64 {
    let new_integrand =
        |x| integrand((contour.parameterization)(x)) * (contour.parameterization_prime)(x);
    integral(new_integrand, &contour.bounds, num_parts)
}

pub fn derivative<T: Fn(Complex64) -> Complex64>(
    f: T,
    z0: Complex64,
    n: Complex64,
    gamma_iterations: usize,
    integral_num_parts: usize,
) -> Complex64 {
    let coefficient = gamma(n + 1., gamma_iterations) / Complex64::new(0., TAU);
    let integrand = |z: Complex64| f(z) / (z - z0).powc(n + 1.);
    let contour = DifferentiableContour {
        parameterization: |t| Complex64::new(t.cos() + z0.re, t.sin() + z0.im),
        parameterization_prime: |t| Complex64::new(-t.sin(), t.cos()),
        bounds: (0., TAU),
    };
    coefficient * contour_integral(integrand, contour, integral_num_parts)
}
