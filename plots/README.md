# Image reference
 - gamma_accuracy.png: testing to see how number of terms affects the accuracy of the Gamma function estimation
 - integral_accuracy.png: testing to see how partition size affects the accuracy of the integral estimation
 - derivative.png: latest output of the derivative grapher
 - 1.png: i-th order derivative of f(z)=z
 - 2.png: 0.9-th order derivative of f(z)=z
 - 3.png: 0.95-th order derivative of f(z)=z
 - 4.png: 0.97-th order derivative of f(z)=z
 - 5.png: 0.99-th order derivative of f(z)=z
 - 6.png: 0.1-th order derivative of f(z)=zs
 - 7.png: 0.4-th order derivative of f(z)=zs
 - 8.png: 0.6-th order derivative of f(z)=zs
 - 9.png: 0.75-th order derivative of f(z)=zs
 - 1.gif: animate from 0-th to 1-st order derivative of f(z)=sin(z)
 - 2.gif: animate from 0-th to i-th order derivative of f(z)=sin(z)
 - 3.gif: animate from 1/2 i-th to 1/2 i-th order derivative of f(z)=sin(z) along the counterclockwise circle with
   radius 1/2 centered at the origin
 - 4.gif: animate from 0-th to 4-th order derivative of f(z)=z^4
 - 5.gif: animate from 0-th to 2-nd order derivative of f(z)=e^z. I'm not sure if the fluctuation is due to floating
   point error or other errors gained from an estimation, or if some non-integer derivatives of e^z are not e^z.
 - 6.gif: animate from 0-th to 2i-th order derivative of f(z)=e^z
